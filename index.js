let express = require('express'),
	fs = require('fs'),
	request = require('request'),
	argv = require('minimist')(process.argv.slice(2)),
	cheerio = require('cheerio'),
	app = express(),
	url = require('url'),
	proxy = require('proxy-express'),
	_ = require('underscore'),
	path = require('path'),
	minify = require('html-minifier').minify,
	cache = require('apicache').middleware,
	package = require('./package.json'),
	server = '',
	protocol = '',
	userAgent = '',
	removeCategories = false;

//add motd
function motd() {
	if (fs.existsSync("./motd.txt")) {
		return fs.readFileSync("./motd.txt", "utf8");
	}
	return "";
}

app.use(require('compression')({
	level: 9
}));

app.use(function(req, res, next) {
	server = req.get('host');
	protocol = req.protocol;
	userAgent = req.headers['user-agent']; //|| 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0';
	console.log("[ >> ]", server, protocol, userAgent);
	next();
});

app.use(cache('5 seconds'));
//health check
app.get('/health-check', (req, res) => res.sendStatus(200));

app.get('/styles', function(req, res) {
	let styles = `@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic);html{font-family:'Open Sans',sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}body{margin:0;}article,aside,details,figcaption,figure,footer,header,main,menu,nav,section,summary{display:block;}audio,canvas,progress,video{display:inline-block;vertical-align:baseline;}audio:not([controls]){display:none;height:0;}[hidden],template{display:none;}a{background-color:transparent;}a:active,a:hover{outline:0;}abbr[title]{border-bottom:1px dotted;}b,strong{font-weight:bold;}dfn{font-style:italic;}h1{font-size:2em;margin:0.67em 0;}mark{background:#ff0;color:#000;}small{font-size:80%;}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline;}sup{top:-0.5em;}sub{bottom:-0.25em;}img{border:0;}svg:not(:root){overflow:hidden;}figure{margin:1em 40px;}hr{box-sizing:content-box;height:0;}pre{overflow:auto;}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em;}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0;}button{overflow:visible;}button,select{text-transform:none;}button,html input[type="button"],input[type="reset"],input[type="submit"]{-webkit-appearance:button;cursor:pointer;}button[disabled],html input[disabled]{cursor:default;}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0;}input{line-height:normal;}input[type="checkbox"],input[type="radio"]{box-sizing:border-box;padding:0;}input[type="number"]::-webkit-inner-spin-button,input[type="number"]::-webkit-outer-spin-button{height:auto;}input[type="search"]{-webkit-appearance:textfield;box-sizing:content-box;}input[type="search"]::-webkit-search-cancel-button,input[type="search"]::-webkit-search-decoration{-webkit-appearance:none;}fieldset{border:1px solid #c0c0c0;margin:0 2px;padding:0.35em 0.625em 0.75em;}legend{border:0;padding:0;}textarea{overflow:auto;}optgroup{font-weight:bold;}table{border-collapse:collapse;border-spacing:0;}td,th{padding:0;}body,button,input,select,textarea{color:#404040;font-size:16px;font-size:1rem;line-height:1.5;}h1,h2,h3,h4,h5,h6{clear:both;}p{margin-bottom:1.5em;}b,strong{font-weight:bold;}dfn,cite,em,i{font-style:italic;}blockquote{margin:0 1.5em;}address{margin:0 0 1.5em;}pre{background:#eee;font-family:"Courier 10 Pitch",Courier,monospace;font-size:15px;font-size:0.9375rem;line-height:1.6;margin-bottom:1.6em;max-width:100%;overflow:auto;padding:1.6em;}code,kbd,tt,var{font-family:Monaco,Consolas,"Andale Mono","DejaVu Sans Mono",monospace;font-size:15px;font-size:0.9375rem;}abbr,acronym{border-bottom:1px dotted #666;cursor:help;}mark,ins{background:#fff9c0;text-decoration:none;}small{font-size:75%;}big{font-size:125%;}html{box-sizing:border-box;}*,*:before,*:after{box-sizing:inherit;}body{background:#fff;}blockquote:before,blockquote:after,q:before,q:after{content:"";}blockquote,q{quotes:"" "";}hr{background-color:#ccc;border:0;height:1px;margin-bottom:1.5em;}ul,ol{margin:0 0 1.5em 3em;}ul{list-style:disc;}ol{list-style:decimal;}li>ul,li>ol{margin-bottom:0;margin-left:1.5em;}dt{font-weight:bold;}dd{margin:0 1.5em 1.5em;}img{height:auto;max-width:100%;}table{margin:0 0 1.5em;width:100%;}button,input[type="button"],input[type="reset"],input[type="submit"]{border:1px solid;border-color:#ccc #ccc #bbb;border-radius:3px;background:#e6e6e6;box-shadow:inset 0 1px 0 rgba(255,255,255,0.5),inset 0 15px 17px rgba(255,255,255,0.5),inset 0 -5px 12px rgba(0,0,0,0.05);color:rgba(0,0,0,.8);font-size:12px;font-size:0.75rem;line-height:1;padding:.6em 1em .4em;text-shadow:0 1px 0 rgba(255,255,255,0.8);}button:hover,input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover{border-color:#ccc #bbb #aaa;box-shadow:inset 0 1px 0 rgba(255,255,255,0.8),inset 0 15px 17px rgba(255,255,255,0.8),inset 0 -5px 12px rgba(0,0,0,0.02);}button:focus,input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,button:active,input[type="button"]:active,input[type="reset"]:active,input[type="submit"]:active{border-color:#aaa #bbb #bbb;box-shadow:inset 0 -1px 0 rgba(255,255,255,0.5),inset 0 2px 5px rgba(0,0,0,0.15);}input[type="text"],input[type="email"],input[type="url"],input[type="password"],input[type="search"],textarea{color:#666;border:1px solid #ccc;border-radius:3px;}input[type="text"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="password"]:focus,input[type="search"]:focus,textarea:focus{color:#111;}input[type="text"],input[type="email"],input[type="url"],input[type="password"],input[type="search"]{padding:3px;}textarea{padding-left:3px;width:100%;}a{color:royalblue;}a:visited{color:purple;}a:hover,a:focus,a:active{color:midnightblue;}a:focus{outline:thin dotted;}a:hover,a:active{outline:0;}.site-main .comment-navigation{margin:0 0 1.5em;overflow:hidden;}.comment-navigation .nav-previous{float:left;width:50%;}.comment-navigation .nav-next{float:right;text-align:right;width:50%;}.screen-reader-text{clip:rect(1px,1px,1px,1px);position:absolute!important;height:1px;width:1px;overflow:hidden;}.screen-reader-text:hover,.screen-reader-text:active,.screen-reader-text:focus{background-color:#f1f1f1;border-radius:3px;box-shadow:0 0 2px 2px rgba(0,0,0,0.6);clip:auto!important;color:#21759b;display:block;font-size:14px;font-size:0.875rem;font-weight:bold;height:auto;left:5px;line-height:normal;padding:15px 23px 14px;text-decoration:none;top:5px;width:auto;z-index:100000;}.alignleft{display:inline;float:left;margin-right:1.5em;}.alignright{display:inline;float:right;margin-left:1.5em;}.aligncenter{clear:both;display:block;margin-left:auto;margin-right:auto;}.clear:before,.clear:after,.entry-content:before,.entry-content:after,.comment-content:before,.comment-content:after,.site-header:before,.site-header:after,.site-content:before,.site-content:after,.site-footer:before,.site-footer:after{content:"";display:table;}.clear:after,.entry-content:after,.comment-content:after,.site-header:after,.site-content:after,.site-footer:after{clear:both;}.widget{margin:0 0 1.5em;}.widget select{max-width:100%;}.widget_search .search-submit{display:none;}.sticky{display:block;}.hentry{margin:0 0 1.5em;}.byline,.updated:not(.published){display:none;}.single .byline,.group-blog .byline{display:inline;}.page-content,.entry-content,.entry-summary{margin:1.5em 0 0;}.page-links{clear:both;margin:0 0 1.5em;}.blog .format-aside .entry-title,.archive .format-aside .entry-title{display:none;}.comment-content a{word-wrap:break-word;}.bypostauthor{display:block;}.comment{margin:10px 0;}.infinite-scroll .posts-navigation,.infinite-scroll.neverending .site-footer{display:none;}.infinity-end.neverending .site-footer{display:block;}.page-content .wp-smiley,.entry-content .wp-smiley,.comment-content .wp-smiley{border:none;margin-bottom:0;margin-top:0;padding:0;}embed,iframe,object{max-width:100%;}.wp-caption{margin-bottom:1.5em;max-width:100%;}.wp-caption img[class*="wp-image-"]{display:block;margin:0 auto;}.wp-caption-text{text-align:center;}.wp-caption .wp-caption-text{margin:0.8075em 0;}.gallery{margin-bottom:1.5em;}.gallery-item{display:inline-block;text-align:center;vertical-align:top;width:100%;}.gallery-columns-2 .gallery-item{max-width:50%;}.gallery-columns-3 .gallery-item{max-width:33.33%;}.gallery-columns-4 .gallery-item{max-width:25%;}.gallery-columns-5 .gallery-item{max-width:20%;}.gallery-columns-6 .gallery-item{max-width:16.66%;}.gallery-columns-7 .gallery-item{max-width:14.28%;}.gallery-columns-8 .gallery-item{max-width:12.5%;}.gallery-columns-9 .gallery-item{max-width:11.11%;}.gallery-caption{display:block;}*{padding:0;margin:0;}body.custom-background{background:#BFBFBF!important;}.site-branding{display:none;}#subMain{display:none;}#site-navigation a{color:#fff;text-decoration:none!important;}#site-navigation{width:100%;height:58px;font-size:14px;line-height:58px;text-transform:uppercase;background:#5175e3;}#site-navigation ul{margin:0 0 0 0;}#site-navigation ul li{background:url("/wp-content/themes/wuxia-world/images/line.png") no-repeat center right;margin:0 0px 0 7px;padding:0 7px 0 0;float:left;list-style:none;}.sub-menu{background:#fff;width:100%;display:none;}#site-navigation ul li ul li{height:35px;line-height:35px;position:relative;left:-12px;padding-left:12px;background:#fff;background-image:none!important;}.main-navigation ul ul a{color:#000!important;height:100%;width:100%;display:block!important;}.main-navigation ul ul li{border-bottom:1px solid #ccc;border-right:1px solid #ccc;float:none!important;background:#fff;overflow:hidden;}#site-navigation ul li ul{position:absolute;top:58px;left:0px;z-index:999!important;}.logged-in #site-navigation ul li ul{top:104px;}#site-navigation ul li:last-child{background:none;}#site-navigation .menu-toggle{display:none!important;}.menu-main-menu-container,#menu-main-menu{display:block!important;}#main{margin:0!important;padding:0!important;}.post{border-bottom:1px solid #c9c9c9;padding:5px 10px;background:#fff;position:relative;}body.blog article .entry-content{}body.blog article{position:relative;margin:5px!important;}body.blog article .entry-title{font-size:24px;line-height:24px;margin:30px 0 30px 0;}.single .entry-title{font-size:24px;line-height:24px;margin:20px 0 -20px 0;}.comments-title,#reply-title{font-size:18px;line-height:18px;margin-bottom:10px;}.comment-metadata a{text-decoration:none;}body.blog article .entry-title a{text-decoration:none;}body.forum article,body.page article,.comments-area,article.topic,article.post,article.forum{position:relative;margin:5px!important;background:#fff;}.entry-header h1 a{border:none;}.entry-meta{position:absolute;top:0;}.entry-footer{position:absolute;bottom:10px;}.entry-footer .cat-links{display:none;}.single .entry-content{margin-bottom:40px;}#btnContainer{height:46px;width:100%;position:relative;bottom:0px;margin:-10px 0 0px 0;}#btnContainer div{}.single .chapterBtn{height:46px;position:relative;bottom:50px;margin:0px 0 -40px 0;}.chapterBtn{float:right;background:rgba(51,48,48,0.9);width:45%;}.articleBtn{float:left;}.chapterBtn a,.articleBtn a{color:#fff;height:100%;width:100%;display:inline-block;line-height:46px;text-align:center;text-decoration:none;}#simple_progress_bar{font-size:12px!important;}#bar-container{width:80%;height:100%;background:#fff;border:1px solid #000;}#bar{height:100%;background:#2EFE2E;}#bar-status{display:block;height:30px;width:100%;}#bar-status div{float:left;}#bar-start{width:8%;}#bar-target{width:12%;text-align:right;}#bar-total,#bar-queue{width:100%;text-align:center;font-size:14px;}#bar-paypal{width:100%;text-align:center;}.bar-red{background:#FA5858!important;}.widget{background:#fff;margin:5px!important;}.widget-title{font-size:24px;line-height:24px;}#secondary aside{padding:10px!important;border-bottom:1px solid #ccc;}#secondary aside:last-child{border:none!important;}#supportCD{text-align:center;}footer{position:relative;width:100%;bottom:0;}footer .site-info{background:rgba(51,48,48,0.9);height:46px;text-align:center;color:#fff;padding:10px 0;}footer .site-info a,footer .sep{display:none;}.error-404,.type-page,.comments-area,.type-topic,.type-forum,.type-post{padding:10px;}.nav-links{width:95%;display:block;overflow:hidden;margin:0px 0 0 8px;}.nav-Previous,.nav-Next{background:rgba(51,48,48,0.9);width:45%;float:left;}.nav-Previous a,.nav-Next a{color:#fff;height:100%;width:100%;display:inline-block;line-height:46px;text-align:center;text-decoration:none;}.nav-Next{text-align:right;float:right;}aside.horizontal{width:100%;padding:10px;text-align:center;background:#fff;border-bottom:1px solid #ccc;margin:5px;}.wp-editor-container{border:1px solid #ccc;}.widget_wp_user_stylesheet_switcher_widgets{display:none!important;}#comments{overflow:hidden!important;}.comment-list .avatar{margin:0 10px 0 0px;}#category-popup,.subscription-top{display:none!important;}.right{float:right;}.stacked-list{width:99%;padding:0;margin:0 auto;border:1px solid #e0e0e0;border-radius:2px;overflow:hidden;position:relative;}.stacked-list-item{background-color:#fff;line-height:1.5rem;padding:10px 20px;margin:0;border-bottom:1px solid #e0e0e0;list-style-type:none;}.stacked-list-item.collapsible{padding:0;}.stacked-list-item .collapsible-header{padding:10px 20px;display:table;width:100%;}.stacked-list-item .collapsible-header .title{font-weight:bold;}.stacked-list-item .collapsible-body{max-height:0px;overflow:hidden;-webkit-transition:max-height 0.1s;-moz-transition:max-height 0.1s;-ms-transition:max-height 0.1s;-o-transition:max-height 0.1s;transition:max-height 0.1s;}.stacked-list-item.active .collapsible-body{-webkit-transition:max-height 1s;-moz-transition:max-height 1s;-ms-transition:max-height 1s;-o-transition:max-height 1s;transition:max-height 1s;}.modal{display:none;position:fixed;left:0;right:0;background-color:#fafafa;padding:0;max-height:70%;width:55%;margin:auto;overflow-y:auto;border-radius:2px;will-change:top,opacity;}.modal.bottom-sheet{top:auto;bottom:-100%;margin:0;width:100%;max-height:70%;border-radius:0;overflow-x:hidden;will-change:bottom,opacity;}.backdrop{position:fixed;height:125%;width:100%;top:-25%;left:0px;background-color:black;}@media screen and (max-width: 600px) {#wpadminbar{position:fixed;}}`;
	res.setHeader('Content-Type', 'text/css');
	return res.send(styles);
});

app.use(proxy('m.wuxiaworld.com', {
	request: {
		forceHttps: true,
		followRedirects: true,
		headers: {
			'User-Agent': userAgent,
			'Host': 'm.wuxiaworld.com',
			'Referer': 'http://m.wuxiaworld.com/',
		}
	},
	post: function(proxyObj, callback) {
		// proxyObj contains 
		// { 
		//   req      : Object // express request 
		//   res      : Object // express request 
		//   proxyObj : Object // object used in the 'request' module request 
		//   result   : { 
		//     response : Object, // response object from the proxied request 
		//     body     : Mixed // response body from the proxied request 
		//   } 
		// } 

		let isJSON = (proxyObj.req.query['json']) ? true : false;

		try {

			var $ = cheerio.load(proxyObj.result.body);

			var adverts = ['#gglcptch-css', '#bbspoiler-css', '#collapseomatic-css-css', '#jetpack_css-css', '#ai_widget-30', '#text-59', '#ai_widget-28', '#ai_widget-34', '#ai_widget-39', '#ai_widget-22', '#ai_widget-23', '#text-10', '#ai_widget-38', '.ai-viewport-3'];
			var to_be_removed = ['#text-56', '[rel=canonical]', '#meta-2', '#text-38', '#text-39', 'script', 'style', 'meta', 'noscript', '.banner_ad', '.widget_simple_progress_bar'];
			var links = ['dns-prefetch', 'EditURI', 'wlwmanifest', 'alternate', 'shortlink', 'publisher', 'author', 'profile', 'pingback', 'manifest', 'mask-icon', 'icon', 'apple-touch-icon'];
			var css_contains = ['cookieconsent', 'font-awesome', 'style-mobile'];

			to_be_removed.forEach(item => {
				$(item).remove();
			});

			//remove adverts
			adverts.forEach(advert => {
				$(advert).remove();
			});

			links.forEach(link => {
				$('link[rel=' + link + ']').remove();
			});

			$('link[rel=stylesheet]').each(function(key, link) {
				var href = $(link).attr('href');
				css_contains.forEach(css => {
					if (String(href).includes(css)) {
						$(link).remove();
					}
				});
			});

			//misc
			$('link[rel^=http]').remove();

			//cleanup
			$('#comments').empty().html(`<center><cite>Go to Wuxiaworld to Comment</cite></center>`);
			$('#simple_progress_bar-15').empty().html(`<center><cite>Go to Wuxiaworld for Donations</cite></center>`);
			$('#categories-3').empty().html('<center><cite>Category list removed in lite version.</cite></center>');


			//recent posts
			var recent = $('#recent-posts-2');
			recent.find('ul').css({
				margin: '0px',
				'margin-left': '1px',
				'list-style-type': 'none'
			});

			recent.find('ul li').css({
				padding: '5px',
				'margin-bottom': '2px',
				background: '#666'
			}).find('a').css({
				'text-decoration': 'none',
				color: 'white'
			});


			//remove all links pointing to wuxiaworld
			$('a').each(function(key, link) {
				var href = $(link).attr('href');
				if (typeof href != 'undefined') {
					var _url = url.parse(href, true);
					if (String(_url.hostname).includes('wuxiaworld')) {
						_url.host = server;
						$(link).attr('href', url.format(_url));
					} else {
						$(link).remove();
					}
				}
			});

			$('[src]').each(function(key, img) {
				var src = $(img).attr('src');
				if (typeof src != 'undefined') {
					var _url = url.parse(src, true);
					if (String(_url.hostname).includes('wuxiaworld')) {
						_url.host = server;
						_url.protocol = protocol;
						_src = url.format(_url);
						$(img).attr('src', _src).attr('srcset', _src);
					} else {
						$(img).remove();
					}
				}
			});


			//re-add the viewport and charset
			$('html').prepend('<link rel="stylesheet" href="/styles" />');
			$('html').prepend('<meta name="viewport" content="width=device-width, initial-scale=1">');
			$('html').prepend('<meta charset="UTF-8">');


			//additional styling
			$('head').append(`
                <style>
                    .features {
                        font-size:12px;
                    }
                    .ww_action_click .right {
                        background-color: #eee;
                        padding : 4px 10px;
                        border-radius : 3px;
                    }
                    html.nightmode .widget .widget-title,
                    html.nightmode .featured-title,
                    html.nightmode .chapterBtn a {
                        color:white;
                        background:none;
                    }
                    .nightm {
                        display: inline-block;
                        width: 25%;
                        float: left;
                    }
                    #fontcontrols .nightm{ 
                        margin-top:12px;
                    }
                   .nightmode pre {
                        background: #ccc;
                        color: #303030;
                    }
                </style>
            `);

			var controls = $('#text-41');
			var controls_title = controls.find('h1');

			controls_title.html('Welcome to WW <sup class="badge">lite</sup> <small>v' + package.version + '</small>' + '<pre style="margin-top:12px">' + motd() + '</pre>');
			controls_title.parent().append(`
                <button class="nightm" id="IncreaseFont" onclick="increaseFont()"><i class="fa fa-moon-o" aria-hidden="true"></i>➕</button>
                <button class="nightm" id="DecreaseFont" onclick="decreaseFont()"><i class="fa fa-sun-o" aria-hidden="true"></i>➖</button>
            `);
			controls.append('<div style="clear:both"></div>');
			controls.find('[onclick="enableNightmode()"]').text('☀');
			controls.find('[onclick="disableNightmode()"]').text('🌙');
			$('.collapsible').attr('data-expanded', 'false');

			$('head')
				.append(`<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-99075341-1', 'auto');
                  ga('send', 'pageview');

                </script>`)
				.append(`<script src="https://cdnjs.cloudflare.com/ajax/libs/jslite/1.1.12/JSLite.min.js"></script>`)
				.append(`<script>
                function humanizeElapsedTimestamp(milliseconds) {
                    function numberEnding(number) {
                        return (number > 1) ? 's' : ''; }
                    var temp = Math.floor(milliseconds / 1000);
                    var years = Math.floor(temp / 31536000);
                    if (years) {
                        return years + ' year' + numberEnding(years) + ' ago'; }
                    var days = Math.floor((temp %= 31536000) / 86400);
                    if (days) {
                        return days + ' day' + numberEnding(days) + ' ago'; }
                    var hours = Math.floor((temp %= 86400) / 3600);
                    if (hours) {http://localhost:8080/mobile-active-index/
                        return hours + ' hour' + numberEnding(hours) + ' ago'; }
                    var minutes = Math.floor((temp %= 3600) / 60);
                    if (minutes) {
                        return minutes + ' minute' + numberEnding(minutes) + ' ago'; }
                    var seconds = temp % 60;
                    if (seconds) {
                        return seconds + ' second' + numberEnding(seconds) + ' ago'; }
                    return 'just now';
                }

                function enableNightmode(){
                    document.querySelector('html').className = "nightmode";
                    localStorage.setItem('nightmode', 'y');
                }

                function disableNightmode(){
                    document.querySelector('html').className = "";
                    localStorage.setItem('nightmode', 'n');
                }

                function increaseFont(){
                    var fontSize = $('[itemprop="articleBody"]').css('fontSize');
                    if(fontSize == ""){
                        fontSize = '16px';
                    }

                    $('[itemprop="articleBody"]').css('fontSize', (+fontSize.replace('px', '') + 1) +'px');

                    localStorage.setItem('fontSize', $('[itemprop="articleBody"]').css('fontSize'));
                }
                function decreaseFont(){
                    var fontSize = $('[itemprop="articleBody"]').css('fontSize');
                    if(fontSize == ""){
                        fontSize = '16px';
                    }

                    $('[itemprop="articleBody"]').css('fontSize', (+fontSize.replace('px', '') - 1) +'px');
                    localStorage.setItem('fontSize', $('[itemprop="articleBody"]').css('fontSize'));
                }

                $(document).ready(function(e){
                    $('.collapsible-header').click(function(e){
                        if(!$(this).data('expanded')){
                            $(this).attr('data-expanded', 'true');
                            $(this).parent().find('.collapsible-body').removeAttr('style').css({
                                'max-height' : '9999px',
                            });
                        } else {
                            $(this).attr('data-expanded', 'false');
                            $(this).parent().find('.collapsible-body').css({
                                'max-height' : '0px',
                                'overflow' : 'hidden'
                            });
                        }
                    });
                    $.fn.extend({
                        elapsed: function(options) {
                            this.each(function() {
                                var release_time = $(this).data('releasetime');
                                if (!release_time) {
                                    return this; }
                                var elapsed_time = new Date().getTime() - release_time * 1000;
                                $(this).html(humanizeElapsedTimestamp(elapsed_time));
                                if (elapsed_time <= 6 * 3600 * 1000) { $('<i style="margin-right: 5px;" class="ww_elapsed_if fa fa-exclamation-circle"></i>').insertBefore($(this)); }
                            });
                            return this;
                        }
                    });
                    $('.ww_elapsed_time').elapsed();

                    if(localStorage.getItem('nightmode') == 'y'){
                        $('html').addClass('nightmode');
                    }

                    if(localStorage.getItem('fontSize') != null){
                        if(window.location.href.includes('-index')){
                            $('[itemprop="articleBody"]').css('fontSize', localStorage.getItem('fontSize'));    
                        }
                    }
                });
            </script>`);

			//ver2.0 fix
			$('.collapseomatic_content').removeAttr('style');
			$('.collapseomatic').remove();

			//remove footer

			let siteInfo = $('.site-info');
			siteInfo.find('span').remove();
			siteInfo.text('Proxy Provided for Free');

			let src = $.html();

			try {
				// src = minify($.html(), {
				//     html5: true,
				//     removeAttributeQuotes: true,
				//     removeTagWhitespace: true,
				//     removeScriptTypeAttributes: true,
				//     removeComments: true,
				//     collapseWhitespace: true,
				//     minifyURLs: true,
				//     minifyJS: true,
				//     minifyCSS: true,
				//     removeStyleLinkTypeAttributes: true
				// });
			} catch (ex) {
				var COMMENT_PSEUDO_COMMENT_OR_LT_BANG = new RegExp(
					'<!--[\\s\\S]*?(?:-->)?' + '<!---+>?' // A comment with no body
					+
					'|<!(?![dD][oO][cC][tT][yY][pP][eE]|\\[CDATA\\[)[^>]*>?' + '|<[?][^>]*>?', // A pseudo-comment
					'g');
				src = src.replace(/\r?\n|\r/g, '').replace(/\s+/g, ' ').trim().replace(COMMENT_PSEUDO_COMMENT_OR_LT_BANG, ' ');
			}


			//filter src if categories_page only 
			if (isJSON) {
				$ = cheerio.load(src);
				var remove = ['meta', 'head', 'header', 'footer', '.ai-viewport-1', '#text-41', '#recent-posts-2', '#categories-3'];

				_.each(remove, function(elem) {
					$(elem).remove();
				});

				let categories = [];

				_.each($('a'), function(item) {
					let index = $(item).attr('href')
						.replace(/(http|https)\:\//ig, '')
						.replace(HOST + ((PORT == 80) ? '' : ':' + PORT) + '/', '')
						.replace(/\//ig, '');

					categories.push({
						title: $(item).text().replace(/\(.+\)/ig, ''),
						index: index,
						source: 'wuxiaworld'
					});

				});

				proxyObj.res.setHeader('Content-Type', 'text/json');
				proxyObj.res.removeHeader('Content-Length');
				proxyObj.res.removeHeader('transfer-encoding');

				src = JSON.stringify(categories);
			}

			proxyObj.result.body = src;
			return callback();

		} catch (ex) {
			return callback();
		}
	}
}));

// let HOST = (argv['host']) ? argv['host'] : 'www.darknorth.ml';
let PORT = process.env.PORT || 8080;

console.log("[ >> ] Application Listening to :" + PORT);
app.listen(PORT, '0.0.0.0');