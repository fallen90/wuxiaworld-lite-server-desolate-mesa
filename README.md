# wuxiaworld-lite-server : desolate-mesa #

`desolate-mesa` are projects I've written when I was starting my path to being a programmer. Some were successful and some didnt have any traction at all, but the experience of learning new things was fun and exciting since you could create cool things and express what you imagine into reality.

### What is this repository for? ###

* This creates a proxy to wuxiaworld.com that removes ads, comments, links
* Add font resizer buttons on the site for those like me that needs larger fonts :)

### How do I get set up? ###

* Clone repo
* Install dependencies - `yarn`
* for deployment run `npm run dev`
* for deployment run `npm run deploy:now` or `HOST=0.0.0.0 npm run setup:pm2 && npm run deploy:pm2`
* Note `HOST` env is required for pm2.
* Make sure that your server host specified in `ecosystem.config.js` has `pm2` installed
* If you dont want to install it manually everytime you spawn a server, you can add the installation part in `pre-deploy` script.

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin