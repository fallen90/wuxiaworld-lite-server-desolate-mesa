module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'wuxiaproxy',
      script    : 'index.js',
      env: {
        PORT: 8080
      },
      env_production : {
        NODE_ENV: 'production',
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'root',
      host : process.env.HOST,
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:fallen90/wuxiaworld-lite-server-desolate-mesa.git',
      ssh_options: ['ForwardAgent=yes'],
      path : '/srv/apps/wuxiaproxy',
      'post-deploy' : 'bash /root/.nvm/nvm.sh && npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
