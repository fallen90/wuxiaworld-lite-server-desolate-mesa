FROM mhart/alpine-node:6
WORKDIR /app
COPY . .
RUN npm install --production
EXPOSE 80
CMD ["node", "index.js"]
